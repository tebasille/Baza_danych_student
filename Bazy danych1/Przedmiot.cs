﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazy_danych1
{
    class Przedmiot
    {
        public int id;
        public string nazwaPrzedmiotu;
        public Przedmiot(int id, string nazwaPrzedmiotu)
        {
            this.id = id;
            this.nazwaPrzedmiotu = nazwaPrzedmiotu;
        }
        public Przedmiot()
        {
        }
    }
}
