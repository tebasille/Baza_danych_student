﻿
namespace Bazy_danych1
{
    partial class PokazSyudenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.szukanieImie = new System.Windows.Forms.Label();
            this.imieShow = new System.Windows.Forms.TextBox();
            this.nazwiskoShow = new System.Windows.Forms.TextBox();
            this.przedmiotShow = new System.Windows.Forms.TextBox();
            this.Back = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Show = new System.Windows.Forms.Button();
            this.editStudent = new System.Windows.Forms.Button();
            this.bazaDanychDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bazaDanychDataSet = new Bazy_danych1.BazaDanychDataSet();
            this.LabelZaliczenie = new System.Windows.Forms.Label();
            this.LabelOcena = new System.Windows.Forms.Label();
            this.LabelPrzedmiot = new System.Windows.Forms.Label();
            this.LabelNazwisko = new System.Windows.Forms.Label();
            this.LabelImie = new System.Windows.Forms.Label();
            this.TBprzedmiot = new System.Windows.Forms.TextBox();
            this.TBsurname = new System.Windows.Forms.TextBox();
            this.TBname = new System.Windows.Forms.TextBox();
            this.pokazanieID = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.ListBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bazaDanychDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bazaDanychDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // szukanieImie
            // 
            this.szukanieImie.AutoSize = true;
            this.szukanieImie.Location = new System.Drawing.Point(12, 68);
            this.szukanieImie.Name = "szukanieImie";
            this.szukanieImie.Size = new System.Drawing.Size(40, 13);
            this.szukanieImie.TabIndex = 1;
            this.szukanieImie.Text = "Imieniu";
            this.szukanieImie.Click += new System.EventHandler(this.label1_Click);
            // 
            // imieShow
            // 
            this.imieShow.Location = new System.Drawing.Point(82, 65);
            this.imieShow.Name = "imieShow";
            this.imieShow.Size = new System.Drawing.Size(100, 20);
            this.imieShow.TabIndex = 2;
            // 
            // nazwiskoShow
            // 
            this.nazwiskoShow.Location = new System.Drawing.Point(82, 99);
            this.nazwiskoShow.Name = "nazwiskoShow";
            this.nazwiskoShow.Size = new System.Drawing.Size(100, 20);
            this.nazwiskoShow.TabIndex = 3;
            // 
            // przedmiotShow
            // 
            this.przedmiotShow.Location = new System.Drawing.Point(82, 137);
            this.przedmiotShow.Name = "przedmiotShow";
            this.przedmiotShow.Size = new System.Drawing.Size(100, 20);
            this.przedmiotShow.TabIndex = 4;
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(697, 406);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(75, 23);
            this.Back.TabIndex = 5;
            this.Back.Text = "Wróć";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nazwisku";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Przedmiocie";
            // 
            // Show
            // 
            this.Show.Location = new System.Drawing.Point(39, 207);
            this.Show.Name = "Show";
            this.Show.Size = new System.Drawing.Size(127, 23);
            this.Show.TabIndex = 8;
            this.Show.Text = "Pokaż studenta";
            this.Show.UseVisualStyleBackColor = true;
            this.Show.Click += new System.EventHandler(this.Show_Click);
            // 
            // editStudent
            // 
            this.editStudent.Location = new System.Drawing.Point(569, 272);
            this.editStudent.Name = "editStudent";
            this.editStudent.Size = new System.Drawing.Size(75, 23);
            this.editStudent.TabIndex = 9;
            this.editStudent.Text = "Edytuj";
            this.editStudent.UseVisualStyleBackColor = true;
            this.editStudent.Click += new System.EventHandler(this.editStudent_Click);
            // 
            // bazaDanychDataSetBindingSource
            // 
            this.bazaDanychDataSetBindingSource.DataSource = this.bazaDanychDataSet;
            this.bazaDanychDataSetBindingSource.Position = 0;
            // 
            // bazaDanychDataSet
            // 
            this.bazaDanychDataSet.DataSetName = "BazaDanychDataSet";
            this.bazaDanychDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LabelZaliczenie
            // 
            this.LabelZaliczenie.AutoSize = true;
            this.LabelZaliczenie.Location = new System.Drawing.Point(471, 228);
            this.LabelZaliczenie.Name = "LabelZaliczenie";
            this.LabelZaliczenie.Size = new System.Drawing.Size(92, 13);
            this.LabelZaliczenie.TabIndex = 20;
            this.LabelZaliczenie.Text = "Sposób zaliczenia";
            // 
            // LabelOcena
            // 
            this.LabelOcena.AutoSize = true;
            this.LabelOcena.Location = new System.Drawing.Point(524, 184);
            this.LabelOcena.Name = "LabelOcena";
            this.LabelOcena.Size = new System.Drawing.Size(39, 13);
            this.LabelOcena.TabIndex = 19;
            this.LabelOcena.Text = "Ocena";
            // 
            // LabelPrzedmiot
            // 
            this.LabelPrzedmiot.AutoSize = true;
            this.LabelPrzedmiot.Location = new System.Drawing.Point(510, 142);
            this.LabelPrzedmiot.Name = "LabelPrzedmiot";
            this.LabelPrzedmiot.Size = new System.Drawing.Size(53, 13);
            this.LabelPrzedmiot.TabIndex = 18;
            this.LabelPrzedmiot.Text = "Przedmiot";
            // 
            // LabelNazwisko
            // 
            this.LabelNazwisko.AutoSize = true;
            this.LabelNazwisko.Location = new System.Drawing.Point(510, 106);
            this.LabelNazwisko.Name = "LabelNazwisko";
            this.LabelNazwisko.Size = new System.Drawing.Size(53, 13);
            this.LabelNazwisko.TabIndex = 17;
            this.LabelNazwisko.Text = "Nazwisko";
            // 
            // LabelImie
            // 
            this.LabelImie.AutoSize = true;
            this.LabelImie.Location = new System.Drawing.Point(520, 68);
            this.LabelImie.Name = "LabelImie";
            this.LabelImie.Size = new System.Drawing.Size(26, 13);
            this.LabelImie.TabIndex = 16;
            this.LabelImie.Text = "Imie";
            // 
            // TBprzedmiot
            // 
            this.TBprzedmiot.Location = new System.Drawing.Point(569, 142);
            this.TBprzedmiot.Name = "TBprzedmiot";
            this.TBprzedmiot.Size = new System.Drawing.Size(100, 20);
            this.TBprzedmiot.TabIndex = 13;
            // 
            // TBsurname
            // 
            this.TBsurname.Location = new System.Drawing.Point(569, 103);
            this.TBsurname.Name = "TBsurname";
            this.TBsurname.Size = new System.Drawing.Size(100, 20);
            this.TBsurname.TabIndex = 12;
            // 
            // TBname
            // 
            this.TBname.Location = new System.Drawing.Point(569, 61);
            this.TBname.Name = "TBname";
            this.TBname.Size = new System.Drawing.Size(100, 20);
            this.TBname.TabIndex = 11;
            // 
            // pokazanieID
            // 
            this.pokazanieID.AutoSize = true;
            this.pokazanieID.ForeColor = System.Drawing.Color.LimeGreen;
            this.pokazanieID.Location = new System.Drawing.Point(524, 339);
            this.pokazanieID.Name = "pokazanieID";
            this.pokazanieID.Size = new System.Drawing.Size(26, 13);
            this.pokazanieID.TabIndex = 21;
            this.pokazanieID.Text = "lsdn";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(127, 295);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(229, 95);
            this.checkedListBox1.TabIndex = 22;
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "zaliczenie",
            "egzamin",
            "zaliczenie na ocenę",
            "projekt"});
            this.comboBox2.Location = new System.Drawing.Point(569, 228);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(106, 21);
            this.comboBox2.TabIndex = 24;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "2",
            "2,5",
            "3",
            "3,5",
            "4",
            "4,5",
            "5"});
            this.comboBox1.Location = new System.Drawing.Point(569, 184);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(106, 21);
            this.comboBox1.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(524, 365);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "lsdn";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Znajdź studenta po:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(520, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Edytuj wybranego studenta:";
            // 
            // PokazSyudenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.pokazanieID);
            this.Controls.Add(this.LabelZaliczenie);
            this.Controls.Add(this.LabelOcena);
            this.Controls.Add(this.LabelPrzedmiot);
            this.Controls.Add(this.LabelNazwisko);
            this.Controls.Add(this.LabelImie);
            this.Controls.Add(this.TBprzedmiot);
            this.Controls.Add(this.TBsurname);
            this.Controls.Add(this.TBname);
            this.Controls.Add(this.editStudent);
            this.Controls.Add(this.Show);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.przedmiotShow);
            this.Controls.Add(this.nazwiskoShow);
            this.Controls.Add(this.imieShow);
            this.Controls.Add(this.szukanieImie);
            this.Name = "PokazSyudenta";
            this.Text = "PokazSyudenta";
            this.Load += new System.EventHandler(this.PokazSyudenta_Load);
            this.Shown += new System.EventHandler(this.form_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.bazaDanychDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bazaDanychDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label szukanieImie;
        private System.Windows.Forms.TextBox imieShow;
        private System.Windows.Forms.TextBox nazwiskoShow;
        private System.Windows.Forms.TextBox przedmiotShow;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private new System.Windows.Forms.Button Show;
        private System.Windows.Forms.Button editStudent;
        private System.Windows.Forms.BindingSource bazaDanychDataSetBindingSource;
        private BazaDanychDataSet bazaDanychDataSet;
        private System.Windows.Forms.Label LabelZaliczenie;
        private System.Windows.Forms.Label LabelOcena;
        private System.Windows.Forms.Label LabelPrzedmiot;
        private System.Windows.Forms.Label LabelNazwisko;
        private System.Windows.Forms.Label LabelImie;
        private System.Windows.Forms.TextBox TBprzedmiot;
        private System.Windows.Forms.TextBox TBsurname;
        private System.Windows.Forms.TextBox TBname;
        private System.Windows.Forms.Label pokazanieID;
        private System.Windows.Forms.ListBox checkedListBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}