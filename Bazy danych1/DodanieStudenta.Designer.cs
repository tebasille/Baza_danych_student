﻿
namespace Bazy_danych1
{
    partial class d
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddStudent = new System.Windows.Forms.Button();
            this.TBname = new System.Windows.Forms.TextBox();
            this.TBsurname = new System.Windows.Forms.TextBox();
            this.TBprzedmiot = new System.Windows.Forms.TextBox();
            this.LabelImie = new System.Windows.Forms.Label();
            this.LabelNazwisko = new System.Windows.Forms.Label();
            this.LabelPrzedmiot = new System.Windows.Forms.Label();
            this.LabelOcena = new System.Windows.Forms.Label();
            this.LabelZaliczenie = new System.Windows.Forms.Label();
            this.Back = new System.Windows.Forms.Button();
            this.showAdd = new System.Windows.Forms.ListBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AddStudent
            // 
            this.AddStudent.Location = new System.Drawing.Point(353, 344);
            this.AddStudent.Name = "AddStudent";
            this.AddStudent.Size = new System.Drawing.Size(105, 23);
            this.AddStudent.TabIndex = 0;
            this.AddStudent.Text = "Dodaj studenta";
            this.AddStudent.UseVisualStyleBackColor = true;
            this.AddStudent.Click += new System.EventHandler(this.AddStudent_Click);
            // 
            // TBname
            // 
            this.TBname.Location = new System.Drawing.Point(353, 131);
            this.TBname.Name = "TBname";
            this.TBname.Size = new System.Drawing.Size(100, 20);
            this.TBname.TabIndex = 1;
            this.TBname.TextChanged += new System.EventHandler(this.TBname_TextChanged);
            // 
            // TBsurname
            // 
            this.TBsurname.Location = new System.Drawing.Point(353, 169);
            this.TBsurname.Name = "TBsurname";
            this.TBsurname.Size = new System.Drawing.Size(100, 20);
            this.TBsurname.TabIndex = 2;
            // 
            // TBprzedmiot
            // 
            this.TBprzedmiot.Location = new System.Drawing.Point(353, 208);
            this.TBprzedmiot.Name = "TBprzedmiot";
            this.TBprzedmiot.Size = new System.Drawing.Size(100, 20);
            this.TBprzedmiot.TabIndex = 3;
            // 
            // LabelImie
            // 
            this.LabelImie.AutoSize = true;
            this.LabelImie.Location = new System.Drawing.Point(321, 134);
            this.LabelImie.Name = "LabelImie";
            this.LabelImie.Size = new System.Drawing.Size(26, 13);
            this.LabelImie.TabIndex = 6;
            this.LabelImie.Text = "Imie";
            // 
            // LabelNazwisko
            // 
            this.LabelNazwisko.AutoSize = true;
            this.LabelNazwisko.Location = new System.Drawing.Point(294, 172);
            this.LabelNazwisko.Name = "LabelNazwisko";
            this.LabelNazwisko.Size = new System.Drawing.Size(53, 13);
            this.LabelNazwisko.TabIndex = 7;
            this.LabelNazwisko.Text = "Nazwisko";
            // 
            // LabelPrzedmiot
            // 
            this.LabelPrzedmiot.AutoSize = true;
            this.LabelPrzedmiot.Location = new System.Drawing.Point(294, 208);
            this.LabelPrzedmiot.Name = "LabelPrzedmiot";
            this.LabelPrzedmiot.Size = new System.Drawing.Size(53, 13);
            this.LabelPrzedmiot.TabIndex = 8;
            this.LabelPrzedmiot.Text = "Przedmiot";
            // 
            // LabelOcena
            // 
            this.LabelOcena.AutoSize = true;
            this.LabelOcena.Location = new System.Drawing.Point(308, 250);
            this.LabelOcena.Name = "LabelOcena";
            this.LabelOcena.Size = new System.Drawing.Size(39, 13);
            this.LabelOcena.TabIndex = 9;
            this.LabelOcena.Text = "Ocena";
            // 
            // LabelZaliczenie
            // 
            this.LabelZaliczenie.AutoSize = true;
            this.LabelZaliczenie.Location = new System.Drawing.Point(255, 294);
            this.LabelZaliczenie.Name = "LabelZaliczenie";
            this.LabelZaliczenie.Size = new System.Drawing.Size(92, 13);
            this.LabelZaliczenie.TabIndex = 10;
            this.LabelZaliczenie.Text = "Sposób zaliczenia";
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(545, 378);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(75, 23);
            this.Back.TabIndex = 11;
            this.Back.Text = "Wróć";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // showAdd
            // 
            this.showAdd.FormattingEnabled = true;
            this.showAdd.Location = new System.Drawing.Point(545, 94);
            this.showAdd.Name = "showAdd";
            this.showAdd.Size = new System.Drawing.Size(243, 212);
            this.showAdd.TabIndex = 12;
            this.showAdd.SelectedIndexChanged += new System.EventHandler(this.showAdd_SelectedIndexChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "2",
            "2,5",
            "3",
            "3,5",
            "4",
            "4,5",
            "5"});
            this.comboBox1.Location = new System.Drawing.Point(353, 247);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(162, 21);
            this.comboBox1.TabIndex = 13;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "zaliczenie",
            "egzamin",
            "zaliczenie na ocenę",
            "projekt"});
            this.comboBox2.Location = new System.Drawing.Point(353, 291);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(162, 21);
            this.comboBox2.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(350, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "label1";
            // 
            // d
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.showAdd);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.LabelZaliczenie);
            this.Controls.Add(this.LabelOcena);
            this.Controls.Add(this.LabelPrzedmiot);
            this.Controls.Add(this.LabelNazwisko);
            this.Controls.Add(this.LabelImie);
            this.Controls.Add(this.TBprzedmiot);
            this.Controls.Add(this.TBsurname);
            this.Controls.Add(this.TBname);
            this.Controls.Add(this.AddStudent);
            this.Name = "d";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.d_Load);
            this.Shown += new System.EventHandler(this.form_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddStudent;
        private System.Windows.Forms.TextBox TBname;
        private System.Windows.Forms.TextBox TBsurname;
        private System.Windows.Forms.TextBox TBprzedmiot;
        private System.Windows.Forms.Label LabelImie;
        private System.Windows.Forms.Label LabelNazwisko;
        private System.Windows.Forms.Label LabelPrzedmiot;
        private System.Windows.Forms.Label LabelOcena;
        private System.Windows.Forms.Label LabelZaliczenie;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.ListBox showAdd;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label1;
    }
}