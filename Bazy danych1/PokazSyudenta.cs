﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bazy_danych1
{
    public partial class PokazSyudenta : Form
    {
        private Metody db;
        public PokazSyudenta()
        {
            InitializeComponent();
            db = new Metody(connectionString);
        }
        private string connectionString = Bazy_danych1.Properties.Settings.Default.BazaDanych2ConnectionString;
        private void Back_Click(object sender, EventArgs e)
        {
          this.Hide();
          Form1 abs = new Form1();
          abs.Show();
        }
        private void editStudent_Click(object sender, EventArgs e)
        {
            if (selectedStudent != null)
            {
                bool isNameValid = TBname.Text.Trim() != "";
                bool isSurnameValid = TBsurname.Text.Trim() != "";
                bool isSubjectValid = TBprzedmiot.Text.Trim() != "";
                bool isGradeValid = comboBox1.Text != "";
                bool isZaliczenieValid = comboBox2.Text != "";
                if (isNameValid && isSurnameValid && isSubjectValid && isGradeValid && isZaliczenieValid)
                {
                    if (TBname.Text.Trim() != selectedStudent.name)
                    {
                        string ZmienioneImie = TBname.Text;
                        int id = selectedStudent.id;
                        db.EditStudentByImie(ZmienioneImie, id);
                        pokazanieID.Text = "Edycja przebiegła pomyślnie";
                        label1.Text = string.Empty;
                    }

                    if (TBsurname.Text.Trim() != selectedStudent.surname)
                    {
                        string ZmienioneNazwisko = TBsurname.Text;
                        int id = selectedStudent.id;
                        db.EditStudentByNazwisko(ZmienioneNazwisko, id);
                        pokazanieID.Text = "Edycja przebiegła pomyślnie";
                        label1.Text = string.Empty;
                    }
                    if (TBprzedmiot.Text.Trim() != selectedStudent.subject)
                    {
                        string ZmienionyPrzedmiot = TBprzedmiot.Text;
                        int id = selectedStudent.id;
                        db.EditStudentBySubject(ZmienionyPrzedmiot, id);
                        pokazanieID.Text = "Edycja przebiegła pomyślnie";
                        label1.Text = string.Empty;
                    }
                    if (comboBox1.Text != selectedStudent.grade)
                    {
                        string ZmienionaOcena = comboBox1.Text;
                        int id = selectedStudent.id;
                        db.EditStudentByGrade(ZmienionaOcena, id);
                        pokazanieID.Text = "Edycja przebiegła pomyślnie";
                        label1.Text = string.Empty;
                    }
                    if (comboBox2.Text != selectedStudent.zaliczenie)
                    {
                        string ZmienioneZaliczenie = comboBox2.Text;
                        int id = selectedStudent.id;
                        db.EditStudentByZaliczenie(ZmienioneZaliczenie, id);
                        pokazanieID.Text = "Edycja przebiegła pomyślnie";
                        label1.Text = string.Empty;
                    }
                }
                else { label1.Text = "Edycja nie przebiegła pomyślnie";pokazanieID.Text = string.Empty; }

            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private List<Student> ListaStudentów = new List<Student>();
        public Student selectedStudent = null;
        private void Show_Click(object sender, EventArgs e)
        {
            /*if (imieShow.Text != "")
            {
                var StudentImie= db.FindStudentsByImie(ImieShow);
                ListaStudentów=StudentImie;
                checkedListBox1.DataSource = StudentImie;
                checkedListBox1.DisplayMember = "FullName";
                imieShow.Text = string.Empty;
                label1.Text = string.Empty;
                pokazanieID.Text = string.Empty;
            }
            else
            {
                if (nazwiskoShow.Text != "")
                {
                    
                    var StudentNazwisko = db.FindStudentsByNazwisko(NazwiskoShow);
                    ListaStudentów = StudentNazwisko;
                    checkedListBox1.DataSource = StudentNazwisko;
                    checkedListBox1.DisplayMember = "FullName";
                    nazwiskoShow.Text = string.Empty;
                    label1.Text = string.Empty;
                    pokazanieID.Text = string.Empty;
                }
                else 
                {
                     
                    var StudentPrzedmiot = db.FindStudentsByPrzedmiot(PrzedmiotShow);
                    ListaStudentów = StudentPrzedmiot;
                    checkedListBox1.DataSource = StudentPrzedmiot;
                    checkedListBox1.DisplayMember = "FullName";
                    przedmiotShow.Text = string.Empty;
                    label1.Text = string.Empty;
                    pokazanieID.Text = string.Empty;
                }
            }*/

            var foundStudents= db.FindStudent(imieShow.Text, nazwiskoShow.Text, przedmiotShow.Text);
            ListaStudentów = foundStudents;
            checkedListBox1.DataSource = foundStudents;
            checkedListBox1.DisplayMember = "FullName";
            label1.Text = string.Empty;
            pokazanieID.Text = string.Empty;
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = checkedListBox1.SelectedIndex;
            if (index >= 0)
            {
                selectedStudent = ListaStudentów[index];
                TBname.Text = selectedStudent.name;
                TBsurname.Text = selectedStudent.surname;
                comboBox1.Text = selectedStudent.grade;
                comboBox2.Text = selectedStudent.sposobZaliczenia;
                TBprzedmiot.Text = selectedStudent.subject;
                
            }
            else selectedStudent = null;

            label1.Text = string.Empty;
            pokazanieID.Text = string.Empty;
        }
        private void form_Shown(object sender, EventArgs e)
        {
            var foundStudents = db.FindStudent("","","");
            ListaStudentów = foundStudents;
            checkedListBox1.DataSource = foundStudents;
            checkedListBox1.DisplayMember = "FullName";
            label1.Text = string.Empty;
            pokazanieID.Text = string.Empty;
        }

        private void PokazSyudenta_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
