﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazy_danych1
{
    public class Student
    {
        public int id { get; private set; }
        public string imie { get; set; }
        public string nazwisko { get; set; }
        public string nazwaPrzedmiotu { get; set; }
        public string ocena { get; set; }
        public string sposobZaliczenia { get; set; }

        
        public string FullName => $" {id}.{imie} {nazwisko} {nazwaPrzedmiotu} {ocena} {sposobZaliczenia}";
        public string Id => $"{id}";
        public string name => $"{imie}";
        public string surname => $"{nazwisko}";
        public string subject => $"{nazwaPrzedmiotu}";
        public string grade => $"{ocena}";
        public string zaliczenie => $"{sposobZaliczenia}";

        public Student(int id, string imie, string nazwisko, string nazwaPrzedmiotu, string ocena, string sposobZaliczenia )
        {
            this.id = id;
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.ocena = ocena;
            this.nazwaPrzedmiotu = nazwaPrzedmiotu;
            this.sposobZaliczenia = sposobZaliczenia;

        }
        public Student()
        {

        }
    }
}
