﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazy_danych1
{
    class Metody
    {
        private string connectionString;

        public Metody(string connectionString1)
        {
            this.connectionString = connectionString1;
        }

        public void AddStudent(Student newStudent)
        {
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string insertString = $"insert into Student (imie,nazwisko,przedmiot,ocena,sposobZaliczenia) values ('{newStudent.imie}','{newStudent.nazwisko}','{newStudent.nazwaPrzedmiotu}','{newStudent.ocena}','{newStudent.sposobZaliczenia}')";
                var cmd = new SqlCommand(insertString, con);
                cmd.ExecuteNonQuery();
            }
        }


        public List<Student> FindStudentsByImie(string imie)
        {
            List<Student> result = new List<Student>();
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string selectString = $"select * from Student where imie = '{imie}' ";

                using (var cmd = new SqlCommand(selectString, con))
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var st = new Student(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));
                        result.Add(st);
                    }
                }
            }
            return result;
        }
        public List<Student> FindStudentsByNazwisko(string nazwisko)
        {
            List<Student> result = new List<Student>();
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string selectString = $"select * from Student where nazwisko = '{nazwisko}' ";

                using (var cmd = new SqlCommand(selectString, con))
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var st = new Student(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));
                        result.Add(st);
                    }
                }
            }
            return result;
        }
        public List<Student> FindStudentsByPrzedmiot(string nazwAprzedmiotu)
        {
            List<Student> result = new List<Student>();
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string selectString = $"select * from Student where przedmiot='{nazwAprzedmiotu}'";

                using (var cmd = new SqlCommand(selectString, con))
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var st = new Student(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));
                        result.Add(st);
                    }
                }
            }
            return result;
        }
        public List<Student> FindStudent(string imie, string nazwisko, string przedmiot)
        {
            List<Student> result = new List<Student>();
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string selectString = $"select * from Student ";
                if (imie != "" || nazwisko != "" || przedmiot != "")
                {
                    selectString += "where ";
                    List<string> elements = new List<string>();
                    if (imie != "")
                    {
                        elements.Add($"imie='{imie}' ");
                    }
                    if (nazwisko != "")
                    {
                        elements.Add($"nazwisko='{nazwisko}' ");
                    }
                    if (przedmiot != "")
                    {
                        elements.Add($"przedmiot='{przedmiot}' ");
                    }
                    for (int i = 0; i < elements.Count; i++)
                    {
                        string element = elements[i];
                        selectString += element;
                        if (i < elements.Count - 1)
                        {
                            selectString += "and ";
                        }
                    }
                }
                using (var cmd = new SqlCommand(selectString, con))
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var st = new Student(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));
                        result.Add(st);
                    }
                }
            }
             return result;
        }
        public void EditStudentByImie(string zmienioneImie, int id)
        {
            
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string update= $"update Student set imie='{zmienioneImie}' where id='{id}'";
                var cmd = new SqlCommand(update, con);
                cmd.ExecuteNonQuery();
            }
        }
        public void EditStudentByNazwisko(string zmienioneNazwisko, int id)
        {
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string update = $"update Student set nazwisko='{zmienioneNazwisko}' where id='{id}'";
                var cmd = new SqlCommand(update, con);
                cmd.ExecuteNonQuery();
            }
        }
        public void EditStudentBySubject(string zmienionyPrzedmiot, int id)
        {
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string update = $"update Student set przedmiot='{zmienionyPrzedmiot}' where id='{id}'";
                var cmd = new SqlCommand(update, con);
                cmd.ExecuteNonQuery();
            }
        }
        public void EditStudentByGrade(string zmienionaOcena, int id)
        {
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string update = $"update Student set ocena='{zmienionaOcena}' where id='{id}'";
                var cmd = new SqlCommand(update, con);
                cmd.ExecuteNonQuery();
            }
        }
        public void EditStudentByZaliczenie(string zmienioneZaliczenie, int id)
        {
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                string update = $"update Student set sposobZaliczenia='{zmienioneZaliczenie}' where id='{id}'";
                var cmd = new SqlCommand(update, con);;
                cmd.ExecuteNonQuery();
            }
        }
    }
}
