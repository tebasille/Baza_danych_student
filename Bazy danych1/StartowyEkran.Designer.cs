﻿
namespace Bazy_danych1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddStudent = new System.Windows.Forms.Button();
            this.ShowStudent = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AddStudent
            // 
            this.AddStudent.Location = new System.Drawing.Point(295, 84);
            this.AddStudent.Name = "AddStudent";
            this.AddStudent.Size = new System.Drawing.Size(153, 23);
            this.AddStudent.TabIndex = 0;
            this.AddStudent.Text = "Dodaj studenta";
            this.AddStudent.UseVisualStyleBackColor = true;
            this.AddStudent.Click += new System.EventHandler(this.AddStudent_Click);
            // 
            // ShowStudent
            // 
            this.ShowStudent.Location = new System.Drawing.Point(295, 143);
            this.ShowStudent.Name = "ShowStudent";
            this.ShowStudent.Size = new System.Drawing.Size(153, 23);
            this.ShowStudent.TabIndex = 1;
            this.ShowStudent.Text = "Pokaż studenta";
            this.ShowStudent.UseVisualStyleBackColor = true;
            this.ShowStudent.Click += new System.EventHandler(this.ShowStudent_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ShowStudent);
            this.Controls.Add(this.AddStudent);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AddStudent;
        private System.Windows.Forms.Button ShowStudent;
    }
}

