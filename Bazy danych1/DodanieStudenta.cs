﻿using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System;

namespace Bazy_danych1
{
    public partial class d : Form
    {
        private Metody db;
        public d()
        {
            InitializeComponent();
            db = new Metody(connectionString);
        }

        private void Back_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 abs = new Form1();
            abs.Show();
        }
        private string connectionString = Bazy_danych1.Properties.Settings.Default.BazaDanych2ConnectionString;
        private void AddStudent_Click(object sender, EventArgs e)
        {
            string dodanyStudent;
            Student osoba = new Student();
            osoba.imie = TBname.Text.Trim();
            osoba.nazwisko = TBsurname.Text.Trim();
            osoba.nazwaPrzedmiotu = TBprzedmiot.Text.Trim();
            osoba.ocena = comboBox1.Text;
            osoba.sposobZaliczenia = comboBox2.Text;
            if (osoba.imie != "" && osoba.nazwisko != "" && osoba.nazwaPrzedmiotu != "" && osoba.ocena != "" && osoba.sposobZaliczenia != "")
            {
                dodanyStudent = $"{osoba.imie} {osoba.nazwisko} {osoba.nazwaPrzedmiotu} {osoba.ocena} {osoba.sposobZaliczenia}";
                showAdd.Items.Add(dodanyStudent);
                db.AddStudent(osoba);
                TBname.Text = string.Empty;
                comboBox1.ResetText();
                TBprzedmiot.Text = string.Empty;
                TBsurname.Text = string.Empty;
                comboBox2.ResetText();
                label1.Text = string.Empty;
            }
            else  label1.Text = "Wypełnij wszystkie dane!"; 
        }

        private void showAdd_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TBname_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
    
            
        }

        private void d_Load(object sender, EventArgs e)
        {

        }
        private void form_Shown(object sender, EventArgs e)
        {
            var foundStudents = db.FindStudent("","","");

            for (int i = 0; i < foundStudents.Count; i++)
            {
                Student student = foundStudents[i];
                string dodanyStudent = $"{student.imie} {student.nazwisko} {student.nazwaPrzedmiotu} {student.ocena} {student.sposobZaliczenia}";
                showAdd.Items.Add(dodanyStudent);
                showAdd.DisplayMember = "FullName";
            }
            label1.Text = string.Empty;
        }
    }
}
